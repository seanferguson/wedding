#!/bin/sh

authbind docker run -p 80:4567 -v `pwd`/google_credentials.json:/files/credentials.json -v `pwd`/wedding.db:/files/wedding.db -v `pwd`/app:/app -it seanferguson/wedding:latest
