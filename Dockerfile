FROM ruby:2.3.0
MAINTAINER Sean Ferguson

RUN apt-get update
RUN apt-get install -y libsqlite3-dev

COPY app/ /app/

WORKDIR /app

ENV GOOGLE_APPLICATION_CREDENTIALS /run/secrets/google_credentials

RUN bundle install
VOLUME /files

EXPOSE 4567

CMD /app/entrypoint.sh
