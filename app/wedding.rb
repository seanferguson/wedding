require 'date'
require 'sinatra/base'
require "sinatra/content_for"
require 'googleauth'
require 'google/apis/sheets_v4'
require "data_mapper"

DataMapper.setup(:default, "sqlite3:///files/wedding.db")

class Anecdote
  include DataMapper::Resource

  property :id,    Serial
  property :name, String
  property :anecdote, Text, lazy: false
end

DataMapper.finalize
DataMapper.auto_upgrade!

class Wedding < Sinatra::Application
  set :bind, "0.0.0.0"

  helpers Sinatra::ContentFor

  get '/' do
    erb :index
  end

  get '/about_us' do
    erb :about_us, locals: { anecdotes: Anecdote.all }
  end

  post '/about_us' do
    Anecdote.create(name: params[:name], anecdote: params[:anecdote])

    erb :about_us, locals: { anecdotes: Anecdote.all }
  end

  get '/wedding_party' do
    erb :coming_soon
  end

  get '/booking' do
    erb :booking
  end

  get '/rsvp' do
    erb :rsvp
  end

  post '/rsvp' do
    Wedding.sheets.append_spreadsheet_value(
      "10I1KJNiAe7GOr3PIIhQIRoOwWbIdTFpl5F7K7LwYcKo", 
      "A1:E100", 
      Google::Apis::SheetsV4::ValueRange.new(
        values: [
          [
            params[:names].join(", "), 
            params[:names].count, 
            params[:reservation_number], 
            Wedding.format_date(params[:arrival_date]), 
            Wedding.format_date(params[:departure_date])
          ]
        ]
      ), 
      value_input_option: "USER_ENTERED", 
    )
    erb :rsvp, locals: {message: "You have successfully RSVP'd for #{params[:names].length}! Can't wait to see you there!"}
  end


  def self.format_date(date_string)
    begin
      date = DateTime.iso8601(date_string)
      date_string = date.strftime("%B %-d, %Y")
    rescue

    end

    date_string
  end

  def self.sheets
    sheets = Google::Apis::SheetsV4::SheetsService.new

    scopes =  ["https://www.googleapis.com/auth/spreadsheets"]
    sheets.authorization = Google::Auth.get_application_default(scopes)

    sheets
  end

  run!
end
