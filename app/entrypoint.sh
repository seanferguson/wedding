#!/bin/bash

if [[ -d $STATIC_FILES_PATH ]]
then
  IFS=","
  for host in $VIRTUAL_HOST
  do
    rm -rf $STATIC_FILES_PATH/$host
    cp -r public $STATIC_FILES_PATH/$host
  done
fi

bundle exec ruby wedding.rb;
